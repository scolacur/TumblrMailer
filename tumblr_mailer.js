var fs = require('fs');
var ejs = require('ejs');
var csvFile = fs.readFileSync("friend_list.csv", "utf8");
var emailTemplate = fs.readFileSync("email_template.ejs", "utf8");
var tumblr = require('tumblr.js')
var client = tumblr.createClient({
  consumer_key: 'xxxxxxxxxxxxxxxxxx',
  consumer_secret: 'xxxxxxxxxxxxxxxxxx',
  token: 'xxxxxxxxxxxxxxxxxx',
  token_secret: 'xxxxxxxxxxxxxxxxxx'
});
var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill('xxxxxxxxxxxxxxxxxx-xxxxx');

/*csvParse takes a CSV file as an argument and 
parses each new line into an array of objects with 4 keys*/
function csvParse(csvFile){
	//this array will store each line in the file
	var lineArray = csvFile.split("\n");
	//remove header
	lineArray = lineArray.slice(1, lineArray.length);	

	var objArray = [];	//this array will store our contact objects

	/*This function takes a string, separates it by commas 
	into an array, maps each array element to a key on a 
	new object, then pushes that object onto an array */
	function mapKeysToObj(element, index, array){	
		var keysArr = element.split(",");
		var resObj = {
			firstName: keysArr[0],
			lastName: keysArr[1],
			numMonthsSinceContact: keysArr[2],
			emailAddress: keysArr[3]
		};
		objArray.push(resObj);
    }
    lineArray.forEach(mapKeysToObj); //call mapKeys on each line in doc
	return objArray;
}

var friendList = csvParse(csvFile);
var latestPosts = []; //This is where we'll store the latest posts

function createEmail(err, blog){
	
	/*This function creates a custom email template for each contact in the
	file, and populates the html template with any tumblr posts I've written
	in the past 7 days.*/

	var today = new Date();
	var lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);

	for (var i = 0; i<blog.posts.length; i++){

		var currentPost = blog.posts[i];
		
		//Get the post's date property		
		var postDate = currentPost.date;
		//make a new formatted date object to ease comparison
		var year = postDate.slice(0,4);
		var month = postDate.slice(5,7)-1; //months start at 0
		var day = postDate.slice(8,10);
		var hour = postDate.slice(11,13);
		var minute = postDate.slice(14,16);
		var second = postDate.slice(17,19);
		//These times were initially in GMT so we need to account for time zone
		var fPostDate = new Date(Date.UTC(year, month, day, hour, minute, second));

		if (fPostDate > lastWeek){

			//quotes don't have a title property, so we have to add one
			if (currentPost.type == "quote") {
				currentPost.title = "A quote from " +blog.posts[i].source.toString();
				currentPost.title = currentPost.title.slice(0, currentPost.title.length-5);
				console.log("Current post: " + currentPost.title + currentPost.post_url);
			}
			//push it into the latest posts array
			latestPosts.push(currentPost);
		}
		//Since the posts are ordered by date, once one is too old, 
		//we know the rest will be too old too.
		else{break;}
	}

	//Customize the template for each friend
    friendList.forEach(function(row){

    var firstName = row["firstName"];
    var numMonthsSinceContact = row["numMonthsSinceContact"];
	
	//Generate the custom template
	var customizedTemplate = ejs.render(emailTemplate, 
                { firstName: firstName,  
                  numMonthsSinceContact: numMonthsSinceContact,
                  latestPosts: latestPosts
                });

	//Send the email to each friend!
	sendEmail(row.firstName, row.emailAddress, "Steve", "stephencolacurcio@gmail.com", "Check out my blog!", customizedTemplate);
	})
	console.log(latestPosts);
}

function sendEmail(to_name, to_email, from_name, from_email, subject, message_html){
    var message = {
        "html": message_html,
        "subject": subject,
        "from_email": from_email,
        "from_name": from_name,
        "to": [{
                "email": to_email,
                "name": to_name
            }],
        "important": false,
        "track_opens": true,    
        "auto_html": false,
        "preserve_recipients": true,
        "merge": false,
        "tags": [
            "Fullstack_Tumblrmailer_Workshop"
        ]    
    };
    var async = false;
    var ip_pool = "Main Pool";
    mandrill_client.messages.send({"message": message, "async": async, "ip_pool": ip_pool}, function(result) {
        // console.log(message);
        // console.log(result);   
    }, function(e) {
        // Mandrill returns the error as an object with name and message keys
        console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
        // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    });
 }

//Actually run the function
client.posts('scolacur.tumblr.com', createEmail);

